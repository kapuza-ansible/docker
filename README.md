# Docker role
## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible docker -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt;\
ln -s ./python2.7 /usr/bin/python"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### download docker role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/docker.git
  name: docker
EOF
echo "roles/docker">>.gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## docker
```bash
cat << 'EOF' > docker_install.yml
---
- name: Install docker
  hosts:
    docker_install
  become: yes
  become_user: root
  tasks:
    - name: Docker
      include_role:
        name: docker
      vars:
        docker_action: docker_install
        docker_arch: "[arch=amd64]"
        docker_ver: "stable"
        docker_mirror: "https://download.docker.com/linux/ubuntu"
EOF
ansible-playbook ./docker_install.yml
```

## docker-machine
```bash
cat << 'EOF' > machine_install.yml
---
- name: Install docker-machine
  hosts:
    machine_install
  become: yes
  become_user: root
  tasks:
    - name: Docker-machine
      include_role:
        name: docker
      vars:
        docker_action: machine_install
        docker_machine_ver: "0.16.2"
#       check version on https://github.com/docker/machine/releases/
EOF
ansible-playbook ./machine_install.yml
```

## docker-cli
```bash
cat << 'EOF' > cli_install.yml
---
- name: Install docker-cli
  hosts:
    cli_install
  become: yes
  become_user: root
  tasks:
    - name: Docker-cli
      include_role:
        name: docker
      vars:
        docker_action: cli_install
        docker_cli_ver: "19.03.13"
#       check version on https://docs.docker.com/engine/installation/binaries/
EOF
ansible-playbook ./cli_install.yml
```

## docker-compose
```bash
cat << 'EOF' > compose_install.yml
---
- name: Install docker-compose
  hosts:
    compose_install
  become: yes
  become_user: root
  tasks:
    - name: Docker-compose
      include_role:
        name: docker
      vars:
        docker_action: compose_install
        docker_compose_ver: "1.27.4"
#       check version on https://github.com/docker/compose/releases
EOF
ansible-playbook ./compose_install.yml
```
